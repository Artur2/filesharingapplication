﻿using System;
using System.Data.Entity;
using System.Threading;
using FileSharingApplication.Account.Models;
using FileSharingApplication.Areas.Share.Models;
using FileSharingApplication.Models.Configurations;
using WebGrease.Css.Extensions;


namespace FileSharingApplication.Models.UnitOfWork
{
    public class DataContext : DbContext
    {
        private int CreatedThreadId { get; set; }

        [ThreadStatic]
        private static DataContext context;

        public DataContext()
            : base("FileSharing")
        {
            CreatedThreadId = Thread.CurrentThread.ManagedThreadId;
        }

        public static DataContext JoinOrCreate()
        {
            if (context == null)
                context = new DataContext();

            context.EnsureThreadSafety();
            return context;
        }

        public static DataContext CreateContextless()
        {
            return context = new DataContext();
        }


        private void EnsureThreadSafety()
        {
            if (!CreatedThreadId.Equals(Thread.CurrentThread.ManagedThreadId))
                throw new InvalidOperationException("DbContext is not thread safe, use it only in single thread");
        }

        private IDbSet<User> _users;

        public IDbSet<User> Users
        {
            get
            {
                if (_users == null)
                    _users = Set<User>();

                return _users;
            }
        }

        private IDbSet<Role> _roles;

        public IDbSet<Role> Roles
        {
            get
            {
                if (_roles == null)
                    _roles = Set<Role>();

                return _roles;
            }
        }

        private IDbSet<FileShare> _fileShares;

        public IDbSet<FileShare> FileShares
        {
            get
            {
                if (_fileShares == null)
                    _fileShares = Set<FileShare>();

                return _fileShares;
            }
        }

        public static void Close()
        {
            if (context != null)
            {
                context.EnsureThreadSafety();
                context = null;
            }
        }

        public void Rollback()
        {
            //todo: Test
            ChangeTracker.Entries()
                .ForEach(x =>
                {
                    if (x.State == EntityState.Added)
                        x.State = EntityState.Deleted;
                    else
                        x.State = EntityState.Unchanged;

                    x.CurrentValues.SetValues(x.OriginalValues);
                });
        }

        public void RefreshObjects()
        {
            ChangeTracker.Entries().ForEach(x => x.CurrentValues.SetValues(x.OriginalValues));
        }

        public void SaveAndRefresh()
        {
            SaveChanges();
            RefreshObjects();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //TODO: test configuration

            modelBuilder.Configurations.Add(new UserEntityConfiguration());
            modelBuilder.Configurations.Add(new RoleEntityConfiguration());

            modelBuilder.Configurations.Add(new FileShareEntityConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}