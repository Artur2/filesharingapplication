﻿using System.Data.Entity.ModelConfiguration;
using FileSharingApplication.Account.Models;

namespace FileSharingApplication.Models.Configurations
{
    public class RoleEntityConfiguration : EntityTypeConfiguration<Role>
    {
        public RoleEntityConfiguration()
        {
            this.HasKey(x => x.Id);
        }
    }
}