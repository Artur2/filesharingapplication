﻿using System.Data.Entity.ModelConfiguration;
using FileSharingApplication.Account.Models;

namespace FileSharingApplication.Models.Configurations
{
    public class UserEntityConfiguration : EntityTypeConfiguration<User>
    {
        public UserEntityConfiguration()
        {
            this.HasKey(x => x.Id);
            this.HasMany(x => x.Roles)
                .WithMany(x => x.Users)
                .Map(x => x.MapLeftKey("UserId").MapRightKey("RoleId")
                .ToTable("UserRoles"));
        }
    }
}