﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity.Migrations;
using FileSharingApplication.Account.Models;
using FileSharingApplication.Models.UnitOfWork;
using Microsoft.AspNet.Identity;

namespace FileSharingApplication.Models.Configurations
{
    public class MigrateToLatestVersionConfiguration : DbMigrationsConfiguration<DataContext>
    {
        public MigrateToLatestVersionConfiguration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DataContext context)
        {
            var administrativeRole = new Role()
            {
                Id = Guid.NewGuid(),
                Name = Role.Administrator,
                Users = new Collection<User>()
            };

            var userRole = new Role()
            {
                Id = Guid.NewGuid(),
                Name = Role.User,
                Users = new Collection<User>()
            };

            context.Roles.AddOrUpdate(x => x.Name, userRole);
            context.Roles.AddOrUpdate(x => x.Name, administrativeRole);

            var admin = new User()
            {
                UserName = "Admin",
                Id = Guid.NewGuid(),
                Roles = new Collection<Role>()
            };

            var hasher = new PasswordHasher();
            admin.Password = hasher.HashPassword("qwerty");
            admin.Roles.Add(administrativeRole);
            context.Users.AddOrUpdate(x => x.UserName, admin);

            base.Seed(context);
        }
    }
}