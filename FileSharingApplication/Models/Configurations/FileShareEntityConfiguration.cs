﻿using System.Data.Entity.ModelConfiguration;
using FileSharingApplication.Areas.Share.Models;

namespace FileSharingApplication.Models.Configurations
{
    public class FileShareEntityConfiguration : EntityTypeConfiguration<FileShare>
    {
        public FileShareEntityConfiguration()
        {
            this.HasKey(x => x.Id);
            this.HasRequired(x => x.Owner);
            this.Property(x => x.Key).IsRequired();
            this.Property(x => x.Bucket).IsRequired();
            this.Property(x => x.CreationDate).IsRequired();
        }
    }
}