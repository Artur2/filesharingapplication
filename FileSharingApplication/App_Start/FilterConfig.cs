﻿using System.Web;
using System.Web.Mvc;
using WebMarkupMin.Mvc.ActionFilters;

namespace FileSharingApplication
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute()); //TODO: add error page(add view with model HandleErrorInfo)
            filters.Add(new MinifyHtmlAttribute());
        }
    }
}
