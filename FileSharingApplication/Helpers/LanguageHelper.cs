﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileSharingApplication.Helpers
{
    public class LanguageHelper
    {
        /// <summary>
        /// Name of cookie which used to store user language
        /// </summary>
        public const string CookieName = "Lang";
        /// <summary>
        /// Default Language (en)
        /// </summary>
        public const string DefaultLanguage = "en";
        /// <summary>
        /// Russian language(ru)
        /// </summary>
        public const string Russian = "ru";

        public static string Current
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies[CookieName];
                if (cookie == null)
                    return DefaultLanguage;

                return cookie.Value;
            }
        }

        public static void Set(string language)
        {
            var cookie = HttpContext.Current.Request.Cookies[CookieName];
            if (cookie == null)
            {
                cookie = new HttpCookie(CookieName, language)
                {
                    Expires = DateTime.MaxValue
                };

                HttpContext.Current.Response.AppendCookie(cookie);
            }
            else
            {
                cookie.Value = language;
                HttpContext.Current.Response.SetCookie(cookie);
            }
        }
    }
}