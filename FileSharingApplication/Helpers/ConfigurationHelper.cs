﻿using System.Configuration;
using System.Web.Configuration;

namespace FileSharingApplication.Helpers
{
    public static class ConfigurationHelper
    {
        public static void EncryptSection(string sectionName)
        {
            var config = WebConfigurationManager.OpenWebConfiguration("~");
            var encryptSection = config.GetSection(sectionName);

            encryptSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
            encryptSection.SectionInformation.ForceSave = true;

            config.Save();
        }

        public static void DecryptSection(string sectionName)
        {
            var config = WebConfigurationManager.OpenWebConfiguration("~");
            var decryptSection = config.GetSection(sectionName);

            decryptSection.SectionInformation.UnprotectSection();
            decryptSection.SectionInformation.ForceSave = true;

            config.Save();
        }
    }
}