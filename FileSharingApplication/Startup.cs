﻿using System.Data.Entity;
using FileSharingApplication;
using FileSharingApplication.Models.Configurations;
using FileSharingApplication.Models.UnitOfWork;
using Microsoft.Owin;
using Owin;


[assembly: OwinStartup(typeof(Startup))]
namespace FileSharingApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext,MigrateToLatestVersionConfiguration>("FileSharing"));

            ConfigureAuth(app);
        }
    }
}
