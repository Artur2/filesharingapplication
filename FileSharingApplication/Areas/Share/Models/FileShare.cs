﻿using System;
using System.IO;
using FileSharingApplication.Account.Models;
using FileSharingApplication.Models;

namespace FileSharingApplication.Areas.Share.Models
{
    public class FileShare : Entity
    {

        public static FileShare CreateNew(string bucketName, User owner, string fileName)
        {
            var fileShare = new FileShare()
            {
                Bucket = bucketName,
                CreationDate = DateTime.Now,
                Owner = owner,
                Id = Guid.NewGuid(),
                Title = fileName
            };
            fileShare.GenerateKey(fileName);

            return fileShare;
        }

        public DateTime CreationDate { get; set; }

        public DateTime? LastDownloadDate { get; set; }

        private string GetUrl(bool ssl)
        {
            return string.Format("http{0}://{1}.s3.amazonaws.com/{2}",
                ssl ? "s" : string.Empty,
                this.Bucket,
                this.Key);
        }

        public string Title { get; set; }

        /// <summary>
        /// Full url at Amazon S3
        /// </summary>
        public virtual string Url
        {
            get { return GetUrl(false); }
        }

        /// <summary>
        /// Full ssl url at Amazon S3
        /// </summary>
        public virtual string SslUrl
        {
            get { return GetUrl(true); }
        }

        /// <summary>
        /// AWS Bucket Name S3
        /// </summary>
        public string Bucket { get; set; }

        /// <summary>
        /// Key/filename from object Request from AWS S3
        /// </summary>
        public string Key { get; set; }

        public virtual User Owner { get; set; }

        public virtual void GenerateKey(string fileName)
        {
            var extension = Path.GetExtension(fileName);
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
            var uniqueChars = Guid.NewGuid().ToString().Replace("-", string.Empty);

            Key = string.Concat(fileNameWithoutExtension, uniqueChars, extension);
        }
    }
}