﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FileSharingApplication.Areas.Share.Models;
using FileSharingApplication.Areas.Share.Services;
using FileSharingApplication.Models.UnitOfWork;


namespace FileSharingApplication.Areas.Share.Controllers
{
    [Authorize]
    public class FileSharesController : Controller
    {
        // GET: Share/FileShares

        public ActionResult Index()
        {
            var context = DataContext.JoinOrCreate();

            var files = context.FileShares
                .Where(x => x.Owner.Id == FileSharingApplication.Account.Models.User.Current.Id);

            return View(files);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(HttpPostedFileBase file)
        {
            var service = AmazonS3Service.CreateNew();

            var share = FileShare.CreateNew("artur2", null, file.FileName);

            return await service.UploadFileAsync(file.InputStream, "artur2", share.Key)
                  .ContinueWith<ActionResult>(x =>
              {
                  if (x.IsCompleted && !x.IsFaulted)
                  {
                      var context = DataContext.CreateContextless();
                      share.Owner = FileSharingApplication.Account.Models.User.Current;
                      context.FileShares.Add(share); // i was right about IEntityChangeTracker 
                      context.SaveChanges();

                      return RedirectToAction("Index");
                  }

                  return RedirectToRoute("Error", new { action = "ServerError" });
              });
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var context = DataContext.JoinOrCreate();

            var share = context.FileShares.Find(id);
            if (share == null)
                return RedirectToRoute("Error", new { action = "NotFound" });

            var service = AmazonS3Service.CreateNew();

            return await service.DeleteFileAsync(share.Bucket, share.Key).ContinueWith(x =>
             {
                 if (x.IsCompleted && !x.IsFaulted)
                 {
                     var dbContext = DataContext.CreateContextless();
                     var sharing = dbContext.FileShares.Find(id);
                     dbContext.FileShares.Remove(sharing);
                     dbContext.SaveChanges();

                     return RedirectToAction("Index");
                 }

                 return RedirectToRoute("Error", new { action = "ServerError" });
             });
        }
    }
}