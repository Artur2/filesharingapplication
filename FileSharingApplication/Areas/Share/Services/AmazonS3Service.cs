﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Configuration;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;


namespace FileSharingApplication.Areas.Share.Services
{
    public class AmazonS3Service
    {

        private IAmazonS3 client;

        protected AmazonS3Service()
        {

        }

        protected AmazonS3Service(string accessKey, string secretKey, RegionEndpoint endpoint)
        {
            client = AWSClientFactory.CreateAmazonS3Client(accessKey, secretKey, endpoint ?? RegionEndpoint.USWest2);
        }

        /// <summary>
        /// Creating new service object 
        /// and using secret key and api key from web.config in sections 
        /// <para>
        /// <c>/configuration/appSettings/add[key="accessKey"]</c>
        /// and <c>/configuration/appSettings/add[key="secretKey"]</c>
        /// </para>
        /// </summary>
        /// <param name="endpoint"><see cref="RegionEndpoint"/> or null(using default endpoint: <c>RegionEndpoint.USWest2</c>)</param>
        /// <returns></returns>
        public static AmazonS3Service CreateNew(RegionEndpoint endpoint = null)
        {
            var accessKey = WebConfigurationManager.AppSettings["accessKey"];
            var secretKey = WebConfigurationManager.AppSettings["secretKey"];

            return CreateNew(accessKey, secretKey, endpoint);
        }

        /// <summary>
        /// Creates new service object
        /// </summary>
        /// <param name="accessKey">access key for aws account, u can use custom users 
        /// from IAM service
        /// </param>
        /// <param name="secretKey">secret key for aws account, u can use custom users 
        /// from IAM service
        /// </param>
        /// <param name="endpoint"><see cref="RegionEndpoint"/>></param>
        /// <returns></returns>
        public static AmazonS3Service CreateNew(string accessKey, string secretKey, RegionEndpoint endpoint = null)
        {
            return new AmazonS3Service(accessKey, secretKey, endpoint);
        }

        public async Task UploadFileAsync(Stream inputStream, string bucketName, string fileName)
        {
            var putRequest = new PutObjectRequest()
            {
                Key = fileName,
                BucketName = bucketName,
                InputStream = inputStream,
                CannedACL = S3CannedACL.PublicRead
            };

            var response = await client.PutObjectAsync(putRequest);

            foreach (var key in response.ResponseMetadata.Metadata.Keys)
            {
                Debug.WriteLine(response.ResponseMetadata.Metadata[key]);
            }
        }

        public async Task<string[]> BucketsAsync()
        {
            var bucketsRequest = new ListBucketsRequest();

            var bucketsList = await client.ListBucketsAsync(bucketsRequest);

            return bucketsList.Buckets.Select(x => x.BucketName).ToArray();
        }

        public async Task DeleteFileAsync(string bucketName, string fileName)
        {
            var deleteObjectRequest = new DeleteObjectRequest()
            {
                BucketName = bucketName,
                Key = fileName
            };

            await client.DeleteObjectAsync(deleteObjectRequest);
        }
    }
}