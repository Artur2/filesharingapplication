﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using FileSharingApplication.Account.Models;
using FileSharingApplication.Models.UnitOfWork;
using Microsoft.AspNet.Identity;

namespace FileSharingApplication.Account.Stores
{
    public class UserStore : IUserPasswordStore<User, Guid>, IRoleStore<Role, Guid>
    {
        private DataContext context;

        public UserStore()
        {
            context = DataContext.JoinOrCreate();
        }

        public void Dispose()
        {

        }

        public Task CreateAsync(User user)
        {
            context.Users.Add(user);

            return Task.FromResult(0);
        }

        public Task UpdateAsync(User user)
        {
            var persistedUser = context.Users.Find(user.Id);
            if (persistedUser == null)
                throw new NullReferenceException("persistedUser");

            context.Entry(persistedUser).CurrentValues.SetValues(user);

            return Task.FromResult(0);
        }

        public Task DeleteAsync(User user)
        {
            context.Users.Remove(user);

            return Task.FromResult(0);
        }

        public Task CreateAsync(Role role)
        {
            context.Roles.Add(role);

            return Task.FromResult(0);
        }

        public Task UpdateAsync(Role role)
        {
            var persistedRole = context.Roles.Find(role.Id);
            if (persistedRole == null)
                throw new NullReferenceException("Role not found");

            context.Entry(persistedRole).CurrentValues.SetValues(role);

            return Task.FromResult(0);
        }

        public Task DeleteAsync(Role role)
        {
            context.Roles.Remove(role);

            return Task.FromResult(0);
        }

        Task<Role> IRoleStore<Role, Guid>.FindByIdAsync(Guid roleId)
        {
            return Task.FromResult(context.Roles.Find(roleId));
        }

        Task<Role> IRoleStore<Role, Guid>.FindByNameAsync(string roleName)
        {
            return Task.FromResult(context.Roles.FirstOrDefault(x => x.Name.Equals(roleName)));
        }

        public Task<User> FindByIdAsync(Guid userId)
        {
            var user = context.Users.Find(userId);

            return Task.FromResult(user);
        }

        public Task<User> FindByNameAsync(string userName)
        {
            var user = context.Users.FirstOrDefault(x => x.UserName.Equals(userName));

            return Task.FromResult(user);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.Password = passwordHash;

            return Task.FromResult(0);
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            return Task.FromResult(user.Password);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return Task.FromResult(!string.IsNullOrWhiteSpace(user.Password));
        }
    }
}