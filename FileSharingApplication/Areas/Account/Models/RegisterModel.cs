﻿using System.ComponentModel.DataAnnotations;

namespace FileSharingApplication.Account.Models
{
    public class RegisterModel
    {
        [Required]
        [DataType(DataType.Text)]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}