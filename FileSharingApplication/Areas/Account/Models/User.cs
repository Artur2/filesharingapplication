﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading;
using FileSharingApplication.Models;
using FileSharingApplication.Models.UnitOfWork;
using Microsoft.AspNet.Identity;

namespace FileSharingApplication.Account.Models
{
    public class User : Entity, IUser<Guid>
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public virtual ICollection<Role> Roles { get; set; }

        public static User Current
        {
            get
            {
                if (Thread.CurrentPrincipal == null || Thread.CurrentPrincipal.Identity == null)
                    return null;

                var name = Thread.CurrentPrincipal.Identity.Name;

                return DataContext.JoinOrCreate().Users.FirstOrDefault(x => x.UserName.Equals(name));
            }
        }
    }
}