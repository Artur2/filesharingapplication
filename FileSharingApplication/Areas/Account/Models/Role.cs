﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FileSharingApplication.Models;
using Microsoft.AspNet.Identity;

namespace FileSharingApplication.Account.Models
{
    public class Role : Entity, IRole<Guid>
    {
        [Required]
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public const string Administrator = "Administrator";
        public const string User = "User";
    }
}