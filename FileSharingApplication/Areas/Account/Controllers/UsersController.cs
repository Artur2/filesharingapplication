﻿using System;
using System.Web;
using System.Web.Mvc;
using FileSharingApplication.Account.Models;
using FileSharingApplication.Account.Stores;
using FileSharingApplication.Models.UnitOfWork;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace FileSharingApplication.Areas.Account.Controllers
{
    public class UsersController : Controller
    {

        private UserManager<User, Guid> _userManager;

        public UserManager<User, Guid> UserManager
        {
            get
            {
                if (_userManager == null)
                    _userManager = new UserManager<User, Guid>(new UserStore());

                return _userManager;
            }
        }

        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        // GET: Account/Users
        public ActionResult Index()
        {
            return View();
        }

        //---------------- TODO --------------- 
        // Add Views

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(string login, string password)
        {
            var user = UserManager.FindByName(login);

            if (user != null &&
                UserManager.HasPassword(user.Id) &&
                UserManager.CheckPassword(user, password))
            {
                var identity = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

                AuthenticationManager.SignIn(new AuthenticationProperties()
                {
                    IsPersistent = true
                }, identity);

                return RedirectToAction("Index", "FileShares", new { area = "Share" });
            }

            ModelState.AddModelError("_FORM", "Check your login or password");

            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult Logoff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return Redirect("/");
        }

        [HttpGet]
        public ActionResult Register()
        {
            var model = new RegisterModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var context = DataContext.JoinOrCreate();

            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = model.Login
            };
            UserManager.Create(user, model.Password);

            var identity = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

            AuthenticationManager.SignIn(new AuthenticationProperties()
            {
                IsPersistent = true
            }, identity);

            context.SaveChanges();

            return Redirect("/");
        }

        [Authorize]
        public ActionResult Cabinet()
        {
            var user = FileSharingApplication.Account.Models.User.Current;

            return View(user);
        }
    }
}