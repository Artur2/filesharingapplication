﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FileSharingApplication.Models.UnitOfWork;
using System.Web.WebPages;
using FileSharingApplication.Helpers;

namespace FileSharingApplication
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //DisplayModeProvider.Instance.Modes.Insert(0, new DefaultDisplayMode("ru")
            //    {
            //        ContextCondition = x => LanguageHelper.Current == LanguageHelper.Russian
            //    });
        }

        protected void Application_EndRequest()
        {
            DataContext.Close();
        }
    }
}
