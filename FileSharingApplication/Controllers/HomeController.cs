﻿using FileSharingApplication.Helpers;
using FileSharingApplication.Models.UnitOfWork;
using System;
using System.Web.Mvc;

namespace FileSharingApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Error()
        {
            throw new NotImplementedException();
        }

        public ActionResult ChangeLanguage(string lang)
        {
            LanguageHelper.Set(lang);

            return Redirect("/");
        }
    }
}